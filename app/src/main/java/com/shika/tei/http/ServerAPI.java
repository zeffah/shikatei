package com.shika.tei.http;

/**
 * Created by zeffah on 2/11/17.
 **/

public class ServerAPI {
    private static String BASE_URL = "http://192.168.1.15:9018/api/";//192.168.43.14//192.168.0.12 jobseeker.niokolee.com
    public static String USER_REGISTER_URL = BASE_URL+"register.php";
    public static String USER_LOGIN_URL = BASE_URL+"login.php";
    public static String EDUCATION_LEVEL_URL = BASE_URL+"qualification_level.php";
    public static String QUALIFICATION_URL = BASE_URL+"qualification.php";
    public static String JOB_LIST_URL = BASE_URL+"job_list.php";
    public static String FILE_UPLOAD_URL = BASE_URL+"uploads.php";
    public static String PRODUCT_CATEGORY_URL = BASE_URL+"productcategories";
}
