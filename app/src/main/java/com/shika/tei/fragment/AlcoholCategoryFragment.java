package com.shika.tei.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shika.tei.R;
import com.shika.tei.common.CommonMethods;
import com.shika.tei.dbServices.OrderDetailService;
import com.shika.tei.model.Page;
import com.shika.tei.util.Helper;

import java.util.ArrayList;
import java.util.List;

public class AlcoholCategoryFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    View layoutView;
    View checkoutStrip;
    public AlcoholCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        CommonMethods.setBottomSheetBehavior(OrderDetailService.getOrderQuantity(getActivity()), checkoutStrip);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        checkoutStrip = getActivity().findViewById(R.id.view_checkout_strip);
        CommonMethods.setBottomSheetBehavior(OrderDetailService.getOrderQuantity(getActivity()), checkoutStrip);
        // Inflate the layout for this fragment
        layoutView = inflater.inflate(R.layout.fragment_alcohol_category, container, false);
        viewPager = (ViewPager) layoutView.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) layoutView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        return layoutView;
    }

    private void setupViewPager(ViewPager viewPager){
        ViewPageAdapter pageAdapter = new ViewPageAdapter(getActivity().getSupportFragmentManager());
        List<Page> pageList = Helper.getPages();
        for (Page page:pageList) {
            pageAdapter.addPage(page);
        }
        viewPager.setAdapter(pageAdapter);
    }

    private class ViewPageAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position){
            return mFragmentTitleList.get(position);
        }

        private void addPage(Page page){
            mFragmentList.add(page.getFragment());
            mFragmentTitleList.add(page.getTitle());
        }

        private void addPage(Fragment page, String title){
            mFragmentList.add(page);
            mFragmentTitleList.add(title);
        }
    }
}
