package com.shika.tei.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shika.tei.R;
import com.shika.tei.adapter.OrderProductsAdapter;
import com.shika.tei.adapter.ProductListAdapter;
import com.shika.tei.common.CommonMethods;
import com.shika.tei.dbServices.OrderDetailService;
import com.shika.tei.model.Order;
import com.shika.tei.model.OrderDetail;
import com.shika.tei.model.Product;
import com.shika.tei.page.MapsActivity;

import java.util.Calendar;
import java.util.List;

public class WhiskeyFragment extends Fragment implements OrderProductsAdapter.UpdateOrderProductQuantity{
    List<Product> productList;
    ListView listViewProducts;
    TextView txtShoppingAmount, txtProductQuantity;
    ProductListAdapter productListAdapter;
    OrderProductsAdapter orderProductsAdapter;

    View bottomSheet, checkoutStrip, listFooter;
    AppCompatButton btnCheckout;
    private BottomSheetBehavior mBottomSheetBehavior;

    public WhiskeyFragment() {
        // Required empty public constructor
    }

    public static WhiskeyFragment newInstance(String param1, String param2) {
        WhiskeyFragment fragment = new WhiskeyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        txtShoppingAmount = (TextView)getActivity().findViewById(R.id.txt_order_total_amount);
        txtProductQuantity = (TextView)getActivity().findViewById(R.id.tv_shopping_quantity);
        bottomSheet = getActivity().findViewById(R.id.bottom_sheet);
        checkoutStrip = getActivity().findViewById(R.id.view_checkout_strip);
        btnCheckout = (AppCompatButton)checkoutStrip.findViewById(R.id.btn_order_checkout);
        btnCheckout.setOnClickListener(viewEventListener);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        CommonMethods.setBottomSheetBehavior(OrderDetailService.getOrderQuantity(getActivity()), checkoutStrip);
    }

    private View.OnClickListener viewEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btn_order_checkout:
                    int totalCheckoutQty = Integer.parseInt(txtProductQuantity.getText().toString().trim());
                    if (totalCheckoutQty > 0){
                        openCheckoutDialog();
                    }else {
                        Toast.makeText(getActivity(), "Add at least one product to basket before checkout!", Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    };

    private void openCheckoutDialog(){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        View alertView = getActivity().getLayoutInflater().inflate(R.layout.order_list_layout, null);
        ListView orderListView = (ListView)alertView.findViewById(R.id.order_list_view);
        initializeListView(fetchOrderDetailList(), orderListView);
        alertBuilder.setView(alertView);
        alertBuilder.setPositiveButton("Order", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                CommonMethods.saveOrder(CommonMethods.orderItemList());
                startActivity(new Intent(getActivity(), MapsActivity.class));
            }
        });
        alertBuilder.create().show();
    }

    private void initializeListView(List<OrderDetail> orderDetail, ListView listView){
        orderProductsAdapter = new OrderProductsAdapter(getActivity(), 0, orderDetail, checkoutStrip, this);
        listView.setAdapter(orderProductsAdapter);
    }

    private List<OrderDetail> fetchOrderDetailList(){
        return OrderDetail.findWithQuery(OrderDetail.class, "SELECT * FROM order_detail");
    }

    @Override
    public void onResume() {
        super.onResume();
        CommonMethods.setBottomSheetBehavior(OrderDetailService.getOrderQuantity(getActivity()), checkoutStrip);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_beer, container, false);
        listViewProducts = (ListView)view.findViewById(R.id.list_view_items);
        listFooter = inflater.inflate(R.layout.list_view_footer, listViewProducts, false);
        listViewProducts.addFooterView(listFooter);
        populateListView(fetchProducts());
        return view;
    }

    private void populateListView(List<Product> productList){
        productListAdapter = new ProductListAdapter(getActivity(), 0, productList, checkoutStrip);
        listViewProducts.setAdapter(productListAdapter);
    }

    private List<Product> fetchProducts(){
        productList = CommonMethods.getProductListByCategory(1);
        return productList;
    }

    @Override
    public void onUpdateQuantity(Product product) {
        productListAdapter.reloadList(CommonMethods.getProductListByCategory(1));
        for (int i = 0; i < productList.size(); i++){
            Product mProduct = productList.get(i);
            if (mProduct.getProductId() == product.getProductId()){
                productList.remove(mProduct);
                productList.add(i, product);
                productListAdapter.notifyDataSetChanged();
                break;
            }
        }
    }
}
