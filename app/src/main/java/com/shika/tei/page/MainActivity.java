package com.shika.tei.page;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shika.tei.R;
import com.shika.tei.adapter.OrderProductsAdapter;
import com.shika.tei.dbServices.OrderDetailService;
import com.shika.tei.fragment.WelcomeFragment;
import com.shika.tei.model.OrderDetail;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    ImageView cart;
    View bottomSheet;
    TextView txtTotalQtyToOrder, txtOrderAmount;
    AppCompatButton btnCheckout;
    OrderProductsAdapter orderProductsAdapter;
    private BottomSheetBehavior mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        cart = (ImageView) this.findViewById(R.id.img_shopping_cart);
        txtTotalQtyToOrder = (TextView)this.findViewById(R.id.tv_shopping_quantity);
        txtOrderAmount = (TextView)this.findViewById(R.id.txt_order_total_amount);
        btnCheckout = (AppCompatButton)this.findViewById(R.id.btn_order_checkout);
//        btnCheckout.setOnClickListener(viewEventListener);
        setSupportActionBar(toolbar);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.app_container, new WelcomeFragment(), "WelcomeFragment")
                .addToBackStack("WelcomeFragment").commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtTotalQtyToOrder.setText(String.valueOf(OrderDetailService.getOrderQuantity(this)));
        txtOrderAmount.setText(String.valueOf(OrderDetailService.getOrderSumAmount(getApplicationContext())));
    }

//    private View.OnClickListener viewEventListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//            switch (view.getId()){
//                case R.id.btn_order_checkout:
//                    int totalCheckoutQty = Integer.parseInt(txtTotalQtyToOrder.getText().toString().trim());
//                    if (totalCheckoutQty > 0){
//                        openCheckoutDialog();
//                    }else {
//                        Toast.makeText(MainActivity.this, "Add at least one product to basket before checkout!", Toast.LENGTH_LONG).show();
//                    }
//                    break;
//            }
//        }
//    };


//    private void openCheckoutDialog(){
//        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
//        View alertView = getLayoutInflater().inflate(R.layout.order_list_layout, null);
//        ListView orderListView = (ListView)alertView.findViewById(R.id.order_list_view);
//        initializeListView(fetchOrderDetailList(), orderListView);
//        alertBuilder.setView(alertView);
//        alertBuilder.setPositiveButton("Order", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Toast.makeText(MainActivity.this, "To open location mapping", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(MainActivity.this, MapsActivity.class));
//            }
//        });
//        alertBuilder.create().show();
//    }

//    private void initializeListView(List<OrderDetail> orderDetail, ListView listView){
//        orderProductsAdapter = new OrderProductsAdapter(this, 0, orderDetail,  txtTotalQtyToOrder, txtOrderAmount);
//        listView.setAdapter(orderProductsAdapter);
//    }

//    private List<OrderDetail> fetchOrderDetailList(){
//        return OrderDetail.findWithQuery(OrderDetail.class, "SELECT * FROM order_detail");
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
