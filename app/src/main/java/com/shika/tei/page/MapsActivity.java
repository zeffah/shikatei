package com.shika.tei.page;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.shika.tei.R;
import com.shika.tei.common.CommonMethods;
import com.shika.tei.model.OrderDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private GoogleMap mMap;
    Location mLastLocation;
    Polyline polyLine;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    View locationSelect;
    AppCompatButton btnChooseLocation;
    Context context;
//    View bottomSheet;
    AutoCompleteTextView autoCompleteTextViewSearchParking;
    TextView txtLocationAddress;
//    ParkingSearchAdapter parkingSearchAdapter;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
//    private BottomSheetBehavior mBottomSheetBehavior;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getApplicationContext();
        setContentView(R.layout.activity_maps);
//        createParkingList();
//        bottomSheet = findViewById(R.id.bottom_sheet);
        locationSelect = findViewById(R.id.location_bottom_sheet);
        btnChooseLocation = (AppCompatButton)locationSelect.findViewById(R.id.btnSelectLocation);
        btnChooseLocation.setOnClickListener(viewClickListener);
        txtLocationAddress = (TextView)this.findViewById(R.id.txt_location_address);
        autoCompleteTextViewSearchParking = (AutoCompleteTextView)findViewById(R.id.parkingSearchAutoCompleteView);
        autoCompleteTextViewSearchParking.setThreshold(1);
//        autoCompleteTextViewSearchParking.setOnItemClickListener(itemClickListener);
//        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
//        mBottomSheetBehavior.setPeekHeight(0);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//           List<OrderDetail> orderDetailList = CommonMethods.getStandingOrder().getOrderItem();
//            Toast.makeText(getApplicationContext(), "No of Items: "+orderDetailList.size(), Toast.LENGTH_SHORT).show();
            JSONObject jObject = new JSONObject();
            List<OrderDetail> mOrderDetailList = OrderDetail.findWithQuery(OrderDetail.class, "SELECT * FROM order_detail");
            try {
                jObject.put("orderName", "mOrder");
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < mOrderDetailList.size(); i++){
                    OrderDetail orderDetail = mOrderDetailList.get(i);
                    JSONObject orderItemObj = new JSONObject();
                    orderItemObj.put("productId", orderDetail.getProductId());
                    orderItemObj.put("quantity", orderDetail.getProductQuantity());
                    orderItemObj.put("productPrice", orderDetail.getProductPrice());
                    jsonArray.put(i, orderItemObj);
                }
                jObject.put("orderItems", jsonArray);
            }catch (JSONException e){
                e.printStackTrace();
            }

            Toast.makeText(MapsActivity.this, ""+jObject.toString(), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, locationListener);
        }
    }

//    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//            SharedMethods.hideKeyboardFrom(getApplicationContext(), autoCompleteTextViewSearchParking);
//            Parking parking = parkingSearchAdapter.getItem(position);
//            if (parking != null) {
//                double latitude = parking.getLatitude();
//                double longitude = parking.getLongitude();
//                LatLng myLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
//                DrawRouteBtnPoints(myLocation, new LatLng(latitude, longitude), parking);
//
//                if(mBottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
//                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                }
//            }
//        }
//    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        LocationManager locationManager = (LocationManager) context
                                .getSystemService(Context.LOCATION_SERVICE);
                        boolean gpsIsEnabled = locationManager
                                .isProviderEnabled(LocationManager.GPS_PROVIDER);
                        if (!gpsIsEnabled){
                            openLocationEnableSettings();
                        }
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

//        List<Parking> mParkingList = createParkingList();
//        if (mParkingList != null && !mParkingList.isEmpty()) {
//            for (Parking mParking : mParkingList) {
//                LatLng latLng = new LatLng(mParking.getLatitude(), mParking.getLongitude());
//                mMap.addMarker(new MarkerOptions().position(latLng));
//            }
//        }
        mMap.setOnMarkerClickListener(markerClickListener);
        mMap.setOnMapClickListener(mapClickListener);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    GoogleMap.OnMarkerClickListener markerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            LatLng markerPosition = marker.getPosition();
            LatLng myLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
//            if(mBottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
//                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//            }
            return false;
        }
    };

    GoogleMap.OnMapClickListener mapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
//            txtLocationAddress.setText(""+latLng.latitude+", "+latLng.longitude);
            getLocationAddress(latLng, txtLocationAddress);
        }
    };

    public void getLocationAddress(LatLng latLng, TextView textView){
        try {
            Geocoder geo = new Geocoder(MapsActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.isEmpty()) {
                textView.setText("Waiting for Location");
            }
            else {
                if (addresses.size() > 0) {
                    textView.setText(addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() +", " + addresses.get(0).getCountryName());
                    //Toast.makeText(getApplicationContext(), "Address:- " + addresses.get(0).getFeatureName() + addresses.get(0).getAdminArea() + addresses.get(0).getLocality(), Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), ""+addresses.get(0).getAddressLine(0), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }
    }

    com.google.android.gms.location.LocationListener locationListener = new com.google.android.gms.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLastLocation = location;
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }
            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("My Location");
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_on_light_blue_700_24dp));
            mCurrLocationMarker = mMap.addMarker(markerOptions);

            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }
    };

//    private void DrawRouteBtnPoints(final LatLng Origin, final LatLng Destination, final Parking parking){
//        String serverKey = "AIzaSyAfInI_A5GeOVEF-kzJ6dD2uLY6CWJqXFQ";
//        GoogleDirection.withServerKey(serverKey)
//            .from(Origin)
//            .to(Destination)
//            .transportMode(TransportMode.DRIVING)
//            .alternativeRoute(true)
//            .execute(new DirectionCallback() {
//                @Override
//                public void onDirectionSuccess(Direction direction) {
//                    mMap.addMarker(new MarkerOptions().position(Destination));
//                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Destination, 16));
//                    ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
//                    PolylineOptions polylineOptions = DirectionConverter.createPolyline(getApplicationContext(), directionPositionList, 5, Color.YELLOW);
//                    if (polyLine != null)
//                        polyLine.remove();
//                    polyLine = mMap.addPolyline(polylineOptions);
//                    Leg leg = direction.getRouteList().get(0).getLegList().get(0);
//                    populateBottomSheet(leg, bottomSheet, parking);
//                }
//
//                @Override
//                public void onDirectionFailure(Throwable t) {
//                    Log.d("DirectionsAPIFailure", t.toString());
//                }
//            })
//        ;
//
//    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    private void openLocationEnableSettings() {
        // show alert dialog if Internet is not connected
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(
                "You need to activate location service to use this feature. Please turn on network or GPS mode in location settings")
                .setTitle("Location")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        }).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

//    private List<Parking> createParkingList(){
//        List<Parking> mList = new ArrayList<>();
//        Parking mParking1 = new Parking();
//        mParking1.setParkingName("Zeffah Parking Area");
//        mParking1.setEmptySlots(100);
//        mParking1.setOwnership("Private");
//        mParking1.setParkingDuration("12 hours");
//        mParking1.setParkingFee(350);
//        mParking1.setTotalSlots(600);
//        mParking1.setLatitude(-1.282903);
//        mParking1.setLongitude(36.824955);
//        mParking1.save();
//
//        Parking mParking2 = new Parking();
//        mParking2.setParkingName("Nairobi Finest Parking zone");
//        mParking2.setEmptySlots(150);
//        mParking2.setOwnership("Private");
//        mParking2.setParkingDuration("6 hours");
//        mParking2.setParkingFee(150);
//        mParking2.setTotalSlots(600);
//        mParking2.setLatitude(-1.278913);
//        mParking2.setLongitude(36.816522);
//        mParking2.save();
//
//        Parking mParking3 = new Parking();
//        mParking3.setParkingName("CarPark XMPP");
//        mParking3.setEmptySlots(150);
//        mParking3.setOwnership("Private");
//        mParking3.setParkingDuration("6 hours");
//        mParking3.setParkingFee(150);
//        mParking3.setTotalSlots(600);
//        mParking3.setLatitude(-1.276639);
//        mParking3.setLongitude(36.826908);
//        mParking3.save();
//
//        Parking mParking4 = new Parking();
//        mParking4.setParkingName("Nairobi CBD vehicle rest center");
//        mParking4.setEmptySlots(150);
//        mParking4.setOwnership("public");
//        mParking4.setParkingDuration("6 hours");
//        mParking4.setParkingFee(150);
//        mParking4.setTotalSlots(600);
//        mParking4.setLatitude(-1.276639);
//        mParking4.setLongitude(36.826908);
//        mParking4.save();
//
//        mList.add(mParking1);
//        mList.add(mParking2);
//        mList.add(mParking3);
//        mList.add(mParking4);
//
//        return mList;
//    }
//
//    private Parking findParkingByCoordinates(double latitude, double longitude){
//        List<Parking> parkingList = Parking.findWithQuery(Parking.class, "SELECT * FROM parking WHERE latitude = ? and longitude = ?", String.valueOf(latitude), String.valueOf(longitude));
//        Parking parking = null;
//        if (parkingList != null && !parkingList.isEmpty()){
//            parking = parkingList.get(0);
//        }
//        return parking;
//    }
}
