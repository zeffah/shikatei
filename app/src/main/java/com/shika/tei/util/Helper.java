package com.shika.tei.util;

import com.shika.tei.fragment.AperitifFragment;
import com.shika.tei.fragment.BeerFragment;
import com.shika.tei.fragment.BrandyFragment;
import com.shika.tei.fragment.ChampagneFragment;
import com.shika.tei.fragment.CognacFragment;
import com.shika.tei.fragment.EneryDrinkFragment;
import com.shika.tei.fragment.ExtraFragment;
import com.shika.tei.fragment.GinFragment;
import com.shika.tei.fragment.LiqueurFragment;
import com.shika.tei.fragment.MixerFragment;
import com.shika.tei.fragment.RumFragment;
import com.shika.tei.fragment.TequilaFragment;
import com.shika.tei.fragment.VodkaFragment;
import com.shika.tei.fragment.WhiskeyFragment;
import com.shika.tei.fragment.WineFragment;
import com.shika.tei.model.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zeffah on 4/14/17.
 **/

public class Helper {
    public static List<Page> getPages(){
        List<Page> mPageList = new ArrayList<>();

        Page mPageBeer = new Page();
        mPageBeer.setFragment(new BeerFragment());
        mPageBeer.setTitle(String.valueOf("Beer"));
        mPageList.add(mPageBeer);

        Page mPageWine = new Page();
        mPageWine.setFragment(new WineFragment());
        mPageWine.setTitle(String.valueOf("Wine"));
        mPageList.add(mPageWine);

        Page mPageWhiskey = new Page();
        mPageWhiskey.setFragment(new WhiskeyFragment());
        mPageWhiskey.setTitle(String.valueOf("Whiskey"));
        mPageList.add(mPageWhiskey);

        Page mPageVodka = new Page();
        mPageVodka.setFragment(new VodkaFragment());
        mPageVodka.setTitle(String.valueOf("Vodka"));
        mPageList.add(mPageVodka);

        Page mPageCognac = new Page();
        mPageCognac.setFragment(new CognacFragment());
        mPageCognac.setTitle(String.valueOf("Cognac"));
        mPageList.add(mPageCognac);

        Page mPageTequila = new Page();
        mPageTequila.setFragment(new TequilaFragment());
        mPageTequila.setTitle(String.valueOf("Tequila"));
        mPageList.add(mPageTequila);

        Page mPageRum = new Page();
        mPageRum.setFragment(new RumFragment());
        mPageRum.setTitle(String.valueOf("Rum"));
        mPageList.add(mPageRum);

        Page mPageGin = new Page();
        mPageGin.setFragment(new GinFragment());
        mPageGin.setTitle(String.valueOf("Gin"));
        mPageList.add(mPageGin);

        Page mPageLiqueur = new Page();
        mPageLiqueur.setFragment(new LiqueurFragment());
        mPageLiqueur.setTitle(String.valueOf("Liqueur"));
        mPageList.add(mPageLiqueur);

        Page mPageBrandy = new Page();
        mPageBrandy.setFragment(new BrandyFragment());
        mPageBrandy.setTitle(String.valueOf("Brandy"));
        mPageList.add(mPageBrandy);

        Page mPageChampagne = new Page();
        mPageChampagne.setFragment(new ChampagneFragment());
        mPageChampagne.setTitle(String.valueOf("Champagne"));
        mPageList.add(mPageChampagne);

        Page mPageAperitif = new Page();
        mPageAperitif.setFragment(new AperitifFragment());
        mPageAperitif.setTitle(String.valueOf("Aperitif"));
        mPageList.add(mPageAperitif);

        Page mPageEnergyDrink = new Page();
        mPageEnergyDrink.setFragment(new EneryDrinkFragment());
        mPageEnergyDrink.setTitle(String.valueOf("Energy Drink"));
        mPageList.add(mPageEnergyDrink);

        Page mPageMixer = new Page();
        mPageMixer.setFragment(new MixerFragment());
        mPageMixer.setTitle(String.valueOf("Mixer"));
        mPageList.add(mPageMixer);

        Page mPageExtra = new Page();
        mPageExtra.setFragment(new ExtraFragment());
        mPageExtra.setTitle(String.valueOf("Extra"));
        mPageList.add(mPageExtra);

        return mPageList;
    }
}
