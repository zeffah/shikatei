package com.shika.tei.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shika.tei.R;
import com.shika.tei.common.CommonMethods;
import com.shika.tei.model.OrderDetail;
import com.shika.tei.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zeffah on 4/14/17.
 **/

public class OrderProductsAdapter extends ArrayAdapter<OrderDetail> {
    private Context context;
    private List<OrderDetail> list, listSuggestions, filteredList;
    View checkoutStrip;
    private TextView txtQuantity, txtAmount;

    private UpdateOrderProductQuantity updateOrderProductQuantity;

    public OrderProductsAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<OrderDetail> list, View checkoutStrip, UpdateOrderProductQuantity updateOrderProductQuantity) {
        super(context, resource, list);
        this.updateOrderProductQuantity = updateOrderProductQuantity;
        this.context = context;
        this.list = list;
        this.filteredList = new ArrayList<>(list);
        this.listSuggestions = list;
        this.checkoutStrip = checkoutStrip;
        this.txtAmount = (TextView) checkoutStrip.findViewById(R.id.txt_order_total_amount);
        this.txtQuantity = (TextView) checkoutStrip.findViewById(R.id.tv_shopping_quantity);
    }

    @Override
    public int getCount() {
        return listSuggestions.size();
    }

    @Override
    public OrderDetail getItem(int position) {
        return listSuggestions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override @NonNull
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            rowView = inflater.inflate(R.layout.order_list_single_row_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.txtProductName = (TextView) rowView.findViewById(R.id.tv_product_name);
            viewHolder.txtProductType = (TextView) rowView.findViewById(R.id.tv_product_type);
            viewHolder.txtProductPrice = (TextView) rowView.findViewById(R.id.tv_product_cost);
            viewHolder.txtOrderQuantity = (TextView)rowView.findViewById(R.id.txt_order_quantity);
            viewHolder.imgAdd = (ImageView) rowView.findViewById(R.id.img_add);
            viewHolder.imgReduce = (ImageView) rowView.findViewById(R.id.img_reduce);
            viewHolder.imgProduct = (ImageView) rowView.findViewById(R.id.product_image);
            viewHolder.imgRemove = (ImageView)rowView.findViewById(R.id.img_remove_from_order);

            rowView.setTag(viewHolder);
        }
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        final OrderDetail orderDetail = getItem(position);
        if (orderDetail != null) {
            final Product product = Product.findById(Product.class, orderDetail.getProductId());
            if (product != null) {
                holder.txtProductName.setText(product.getProductName());
                holder.txtProductType.setText(String.valueOf(product.getProductType()+"ml"));
                holder.txtProductPrice.setText(String.valueOf("kes. "+product.getCurrentPrice()));
                holder.txtOrderQuantity.setText(String.valueOf(orderDetail.getProductQuantity()));

                holder.imgAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String strQuantity = holder.txtOrderQuantity.getText().toString();
                        int intQty = Integer.parseInt(strQuantity);
                        intQty++;
                        holder.txtOrderQuantity.setText(String.valueOf(intQty));
                        String totalQty = txtQuantity.getText().toString().trim();
                        int intTotalQty = Integer.parseInt(totalQty) + 1;
                        txtQuantity.setText(String.valueOf(intTotalQty));

                        String _strAmount = txtAmount.getText().toString().trim();
                        float _intAmount = Float.parseFloat(_strAmount);
                        float productCost = product.getProductPrice();
                        txtAmount.setText(String.valueOf(_intAmount + productCost));

                        orderDetail.setProductQuantity(intQty);
                        orderDetail.save();

                        product.setQuantityCheckout(intQty);
                        product.save();
                        updateOrderProductQuantity.onUpdateQuantity(product);
                    }
                });
                holder.imgReduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String strQuantity = holder.txtOrderQuantity.getText().toString();
                        int intQty = Integer.parseInt(strQuantity);
                        if (intQty > 0) {
                            intQty--;
                            holder.txtOrderQuantity.setText(String.valueOf(intQty));
                            String totalQty = txtQuantity.getText().toString().trim();
                            int intTotalQty = Integer.parseInt(totalQty) - 1;
                            txtQuantity.setText(String.valueOf(intTotalQty));

                            String _strAmount = txtAmount.getText().toString().trim();
                            float _intAmount = Float.parseFloat(_strAmount);
                            float productCost = product.getProductPrice();
                            txtAmount.setText(String.valueOf(_intAmount - productCost));

                            product.setQuantityCheckout(intQty);
                            product.save();
                            updateOrderProductQuantity.onUpdateQuantity(product);

                            orderDetail.setProductQuantity(intQty);
                            orderDetail.save();
                        }
                        if (intQty < 1){
                            removeItem(position);
                            OrderDetail.delete(orderDetail);
                        }
                    }
                });

                holder.imgRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        removeItem(position);
                        product.setQuantityCheckout(0);
                        product.save();
                        OrderDetail.delete(orderDetail);
                        String totalQty = txtQuantity.getText().toString().trim();
                        txtQuantity.setText(String.valueOf(Integer.parseInt(totalQty) - Integer.parseInt(holder.txtOrderQuantity.getText().toString().trim())));
                        double amtToRemove = orderDetail.getProductPrice() * Integer.parseInt(holder.txtOrderQuantity.getText().toString().trim());
                        txtAmount.setText(String.valueOf(Double.parseDouble(txtAmount.getText().toString().trim()) - amtToRemove));
                        updateOrderProductQuantity.onUpdateQuantity(product);
                    }
                });
            }
        }
        return rowView;
    }


    /**List view holder class**/
    private static class ViewHolder {
        ImageView imgProduct, imgAdd, imgRemove, imgReduce;
        TextView txtProductName, txtProductType, txtProductPrice, txtOrderQuantity;
    }

    public void reloadList(List<OrderDetail> items) {
        this.filteredList = items;
        notifyDataSetChanged();
    }

    public OrderDetail removeItem(int position) {
        final OrderDetail model = listSuggestions.remove(position);
        notifyDataSetChanged();
        return model;
    }

    public void addItem(int position, OrderDetail model) {
        list.add(position, model);
        notifyDataSetChanged();
    }

    public void moveItem(int fromPosition, int toPosition) {
        final OrderDetail model = listSuggestions.remove(fromPosition);
        list.add(toPosition, model);
        notifyDataSetChanged();
    }

    public interface UpdateOrderProductQuantity{
        void onUpdateQuantity(Product product);
    }
}
