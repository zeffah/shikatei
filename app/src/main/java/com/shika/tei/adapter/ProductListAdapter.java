package com.shika.tei.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shika.tei.R;
import com.shika.tei.dbServices.OrderDetailService;
import com.shika.tei.model.OrderDetail;
import com.shika.tei.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zeffah on 4/14/17.
 **/

public class ProductListAdapter extends ArrayAdapter<Product> {
    private Context context;
    private List<Product> list, listSuggestions, filteredList;
    private SparseBooleanArray mSelectedItemsIds;
    TextView txtQuantity, txtAmount;
    View checkoutStrip;
    public ProductListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Product> list, View checkoutStrip /* TextView txtQuantity, TextView txtAmount*/) {
        super(context, resource, list);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.list = list;
        this.filteredList = new ArrayList<>(list);
        this.listSuggestions = list;
        this.checkoutStrip = checkoutStrip;
        this.txtAmount = (TextView) checkoutStrip.findViewById(R.id.txt_order_total_amount);
        this.txtQuantity = (TextView) checkoutStrip.findViewById(R.id.tv_shopping_quantity);
    }

    @Override
    public int getCount() {
        return listSuggestions.size();
    }

    @Override
    public Product getItem(int position) {
        return listSuggestions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;

        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            rowView = inflater.inflate(R.layout.beer_list_row_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtProductName = (TextView) rowView.findViewById(R.id.tv_product_name);
            viewHolder.txtProductType = (TextView) rowView.findViewById(R.id.tv_product_type);
            viewHolder.txtProductPrice = (TextView) rowView.findViewById(R.id.tv_product_cost);
            viewHolder.txtOrderQuantity = (TextView)rowView.findViewById(R.id.txt_order_quantity);
            viewHolder.imgAdd = (ImageView) rowView.findViewById(R.id.img_add);
            viewHolder.imgRemove = (ImageView) rowView.findViewById(R.id.img_reduce);
            viewHolder.imgProduct = (ImageView) rowView.findViewById(R.id.product_image);

            rowView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) rowView.getTag();
        }
        final ViewHolder holder = viewHolder;
        final Product product = getItem(position);
        if (product != null) {
            holder.txtProductName.setText(product.getProductName());
            holder.txtProductType.setText(String.valueOf(product.getProductType()+"ml"));
            holder.txtProductPrice.setText(String.valueOf("kes. "+product.getCurrentPrice()));
            holder.txtOrderQuantity.setText(String.valueOf(""+product.getQuantityCheckout()));
//            holder.imgProduct.set
//            rowView.setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4 : Color.TRANSPARENT);

            holder.imgAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkoutStrip.getVisibility() == View.GONE){
                        checkoutStrip.setVisibility(View.VISIBLE);
                    }
                    String strQuantity = holder.txtOrderQuantity.getText().toString();
                    int intQty = Integer.parseInt(strQuantity);
                    intQty++;
                    holder.txtOrderQuantity.setText(String.valueOf(intQty));

                    product.setQuantityCheckout(intQty);
                    product.save();

                    String totalQty = txtQuantity.getText().toString().trim();
                    int intTotalQty = Integer.parseInt(totalQty) + 1;
                    txtQuantity.setText(String.valueOf(intTotalQty));

                    String _strAmount = txtAmount.getText().toString().trim();
                    float _intAmount = Float.parseFloat(_strAmount);
                    double productCost = product.getCurrentPrice();
                    txtAmount.setText(String.valueOf(_intAmount + productCost));

                    OrderDetail orderDetail = OrderDetailService.getOrderDetail(product.getId());
                    if (orderDetail != null){
                        orderDetail.setProductQuantity(intQty);
                        orderDetail.save();
                    }else {
                        orderDetail = new OrderDetail();
                        orderDetail.setProductQuantity(intQty);
                        orderDetail.setProductPrice(product.getCurrentPrice());
                        orderDetail.setProductId(product.getId());
                        orderDetail.save();
                    }
                }
            });
            holder.imgRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String strQuantity = holder.txtOrderQuantity.getText().toString();
                    int intQty = Integer.parseInt(strQuantity);
                    OrderDetail orderDetail = OrderDetailService.getOrderDetail(product.getId());

                    if (intQty > 0){
                        intQty--;
                        holder.txtOrderQuantity.setText(String.valueOf(intQty));

                        product.setQuantityCheckout(intQty);
                        product.save();

                        String totalQty = txtQuantity.getText().toString().trim();
                        int intTotalQty = Integer.parseInt(totalQty) - 1;
                        txtQuantity.setText(String.valueOf(intTotalQty));

                        String _strAmount = txtAmount.getText().toString().trim();
                        float _intAmount = Float.parseFloat(_strAmount);
                        double productCost = product.getCurrentPrice();
                        txtAmount.setText(String.valueOf(_intAmount - productCost));

                        if (orderDetail != null){
                            orderDetail.setProductQuantity(intQty);
                            orderDetail.save();
                        }
                    }
                    if (intQty  == 0){
                        if (orderDetail != null)
                            OrderDetail.delete(orderDetail);
                    }
                }
            });
        }
        return rowView;
    }


    /**List view holder class**/
    private static class ViewHolder {
        ImageView imgProduct, imgAdd, imgRemove;
        TextView txtProductName, txtProductType, txtProductPrice, txtOrderQuantity;
    }

    public void reloadList(List<Product> items) {
        this.filteredList = items;
        notifyDataSetChanged();
    }

    public Product removeItem(int position) {
        final Product model = listSuggestions.remove(position);
        notifyDataSetChanged();
        return model;
    }

    public void addItem(int position, Product model) {
        list.add(position, model);
        notifyDataSetChanged();
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Product model = listSuggestions.remove(fromPosition);
        list.add(toPosition, model);
        notifyDataSetChanged();
    }
}
