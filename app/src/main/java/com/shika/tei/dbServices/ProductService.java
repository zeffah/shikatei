package com.shika.tei.dbServices;

import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.shika.tei.app.AppController;
import com.shika.tei.common.CommonMethods;
import com.shika.tei.http.ServerAPI;
import com.shika.tei.model.Product;
import com.shika.tei.model.ProductCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zeffah on 5/8/17.
 **/

public class ProductService {

    public static void productsRequest() {
        try {
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(ServerAPI.PRODUCT_CATEGORY_URL,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                Log.d("listItems", response.toString());
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject object = response.getJSONObject(i);
                                    long createdDate = object.getLong("createdDate");
                                    int categoryId = object.getInt("categoryId");
                                    String categoryName = object.getString("name");
                                    String imageUrl = object.getString("imageUrl");
                                    String description = object.getString("description");

                                    ProductCategory category = CommonMethods.checkIfCategoryExist(categoryId);
                                    JSONArray productList = object.getJSONArray("productList");
                                    if (category == null) {
                                        category = new ProductCategory();
                                        category.setCategoryId(categoryId);
                                        category.setCreatedDate(createdDate);
                                        category.setCategoryName(categoryName);
                                        category.setImageUrl(imageUrl);
                                        category.setDescription(description);
                                        category.save();

                                        for (int x = 0; x < productList.length(); x++) {
                                            Product product = new Product();
                                            JSONObject productObject = productList.getJSONObject(x);
                                            long productCreatedDate = productObject.getLong("createdDate");
                                            int productId = productObject.getInt("productId");
                                            String productName = productObject.getString("name");
                                            String volume = productObject.getString("volume");
                                            String shortDesc = productObject.getString("shortDescription");
                                            String fullDesc = productObject.getString("fullDescription");
                                            double currentPrice = productObject.getDouble("currentPrice");
                                            String productImageUrl = productObject.getString("imageUrl");

                                            product.setCreatedDate(productCreatedDate);
                                            product.setProductId(productId);
                                            product.setCategoryId(categoryId);
                                            product.setProductName(productName);
                                            product.setProductType(volume);
                                            product.setShortDescription(shortDesc);
                                            product.setFullDescription(fullDesc);
                                            product.setCurrentPrice(currentPrice);
                                            product.setImageUrl(productImageUrl);
                                            product.setQuantityCheckout(0);

                                            product.save();

                                        }
                                    } else {
                                        for (int x = 0; x < productList.length(); x++) {
                                            JSONObject productObject = productList.getJSONObject(x);
                                            long productCreatedDate = productObject.getLong("createdDate");
                                            int productId = productObject.getInt("productId");
                                            String productName = productObject.getString("name");
                                            String volume = productObject.getString("volume");
                                            String shortDesc = productObject.getString("shortDescription");
                                            String fullDesc = productObject.getString("fullDescription");
                                            double currentPrice = productObject.getDouble("currentPrice");
                                            String productImageUrl = productObject.getString("imageUrl");
                                            Product product = CommonMethods.checkIfProductExist(productId);
                                            if (product == null) {
                                                product = new Product();
                                                product.setCreatedDate(productCreatedDate);
                                                product.setProductId(productId);
                                                product.setCategoryId(categoryId);
                                                product.setProductName(productName);
                                                product.setProductType(volume);
                                                product.setShortDescription(shortDesc);
                                                product.setFullDescription(fullDesc);
                                                product.setCurrentPrice(currentPrice);
                                                product.setImageUrl(productImageUrl);
                                                product.setQuantityCheckout(0);

                                                product.save();
                                            }

                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("products get error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    String credentials = "admin:admin";
                    String auth = "Basic "
                            + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", auth);
                    return headers;
                }
            };
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveProduct(JSONObject productObject, Product product, int categoryId){
        try{
            long productCreatedDate = productObject.getLong("createdDate");
            int productId = productObject.getInt("productId");
            String productName = productObject.getString("name");
            String volume = productObject.getString("volume");
            String shortDesc = productObject.getString("shortDescription");
            String fullDesc = productObject.getString("fullDescription");
            double currentPrice = productObject.getDouble("currentPrice");
            String productImageUrl = productObject.getString("imageUrl");

            product.setCreatedDate(productCreatedDate);
            product.setProductId(productId);
            product.setCategoryId(categoryId);
            product.setProductName(productName);
            product.setProductType(volume);
            product.setShortDescription(shortDesc);
            product.setFullDescription(fullDesc);
            product.setCurrentPrice(currentPrice);
            product.setImageUrl(productImageUrl);
            product.setQuantityCheckout(0);

            product.save();

        }catch (JSONException e ){
            e.printStackTrace();
        }
    }
}
