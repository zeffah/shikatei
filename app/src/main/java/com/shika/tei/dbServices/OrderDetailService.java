package com.shika.tei.dbServices;

import android.content.Context;
import android.database.Cursor;

import com.orm.SugarDb;
import com.shika.tei.model.OrderDetail;

import java.util.List;

/**
 * Created by zeffah on 4/15/17.
 **/

public class OrderDetailService {

    public static OrderDetail getOrderDetail(long productId){
        OrderDetail orderDetail = null;
        List<OrderDetail> orderDetailList = OrderDetail.findWithQuery(OrderDetail.class,
                "SELECT * FROM order_detail WHERE product_id = ? ", String.valueOf(productId));
        if (orderDetailList != null && !orderDetailList.isEmpty()){
            orderDetail = orderDetailList.get(0);
        }

        return orderDetail;
    }

    public static int getOrderQuantity(Context context){
        int totalProducts = 0;
        try {
            SugarDb sugarDb = new SugarDb(context);
            Cursor cursor = sugarDb.getDB().
                    rawQuery("SELECT SUM(product_quantity) FROM order_detail", null);
            if (cursor.moveToFirst())
                totalProducts = cursor.getInt(cursor.getColumnIndex(cursor.getColumnName(0)));
            cursor.close();
            return totalProducts;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totalProducts;
    }

    public static float getOrderSumAmount(Context context) {
        float sumValue = 0;
        try {
            SugarDb db = new SugarDb(context);
            Cursor cursor = db.getDB().
                    rawQuery("SELECT SUM(product_quantity * product_price) AS amount FROM order_detail", null);
            if (cursor.moveToFirst())
                sumValue = cursor.getFloat(cursor.getColumnIndex("amount"));
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sumValue;
    }
}
