package com.shika.tei.model;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

/**
 * Created by zeffah on 4/15/17.
 **/

public class OrderDetail extends SugarRecord {
    @Ignore
    String productName, productType;
    private int orderId, productQuantity;
    private long productId;
    private double productPrice;

    public OrderDetail(){}

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }
}
