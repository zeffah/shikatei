package com.shika.tei.model;

import com.orm.SugarRecord;
import com.shika.tei.model.Product;

/**
 * Created by zeffah on 5/8/17.
 */

public class ProductCategory extends SugarRecord {
    int categoryId;
    String categoryName, imageUrl;
    long createdDate;
    String description;

    public ProductCategory(){}

    public int getCategoryId() {
        return categoryId;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
