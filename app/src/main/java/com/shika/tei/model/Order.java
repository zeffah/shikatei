package com.shika.tei.model;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.List;

/**
 * Created by zeffah on 4/15/17.
 **/

public class Order extends SugarRecord{
    @Ignore
    static final int ORDER_STANDING_STATUS = 0;
    @Ignore
    static final int ORDER_INCOMING_STATUS = 1;
    int orderStatus, orderId;
    long OrderDate, orderTime;
    List<OrderDetail> orderItem;

    public Order() {
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public long getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(long orderDate) {
        OrderDate = orderDate;
    }

    public long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(long orderTime) {
        this.orderTime = orderTime;
    }

    public List<OrderDetail> getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(List<OrderDetail> orderItem) {
        this.orderItem = orderItem;
    }
}
