package com.shika.tei.model;

import com.orm.SugarRecord;

/**
 * Created by zeffah on 4/14/17.
 **/

public class Product extends SugarRecord {

    private String productName, fullDescription, shortDescription, imageUrl;
    private int productId, volume, categoryId;
    private double currentPrice;
    private long createdDate;

    private String  productType;
    private float productPrice;
    private int quantityCheckout;

//    long productCreatedDate = productObject.getLong("createdDate");
//    int productId = productObject.getInt("productId");
//    String productName = productObject.getString("name");
//    String volume = productObject.getString("volume");
//    String shortDesc = productObject.getString("shortDescription");
//    String fullDesc = productObject.getString("fullDescription");
//    double currentPrice = productObject.getDouble("currentPrice");
//    String productImageUrl = productObject.getString("imageUrl");

    public Product(){}

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQuantityCheckout() {
        return quantityCheckout;
    }

    public void setQuantityCheckout(int quantityCheckout) {
        this.quantityCheckout = quantityCheckout;
    }
}
