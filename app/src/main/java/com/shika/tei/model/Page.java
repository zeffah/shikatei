package com.shika.tei.model;

import android.support.v4.app.Fragment;

/**
 * Created by zeffah on 4/13/17.
 **/

public class Page {

    Fragment fragment;
    String title;

    public Page() {
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
