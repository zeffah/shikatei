package com.shika.tei.common;

import android.content.Context;
import android.support.design.widget.BottomSheetBehavior;
import android.view.View;
import android.widget.Toast;

import com.shika.tei.R;
import com.shika.tei.dbServices.OrderDetailService;
import com.shika.tei.model.Order;
import com.shika.tei.model.OrderDetail;
import com.shika.tei.model.Product;
import com.shika.tei.model.ProductCategory;

import java.util.List;

/**
 * Created by zeffah on 5/9/17.
 **/

public class CommonMethods {
    public static ProductCategory checkIfCategoryExist(int categoryId){
        ProductCategory productCategory = null;
        List<ProductCategory> productCategories = ProductCategory.findWithQuery(ProductCategory.class, "SELECT * FROM product_category WHERE category_id = ?", String.valueOf(categoryId));
        if (productCategories != null && !productCategories.isEmpty()){
            productCategory = productCategories.get(0);
        }

        return productCategory;
    }

    public static Product checkIfProductExist(int productId){
        Product product = null;
        List<Product> productList = Product.findWithQuery(Product.class, "SELECT * FROM product WHERE product_id = ?", String.valueOf(productId));
        if (productList != null && !productList.isEmpty()){
            product = productList.get(0);
        }
        return product;
    }

    public static List<Product> getProductListByCategory(int categoryId){
        return Product.findWithQuery(Product.class, "SELECT * FROM product WHERE category_id = ?", String.valueOf(categoryId));
    }

    public static void setBottomSheetBehavior(int orderQuantity, View checkout_stripView){
        if (orderQuantity > 0){
            checkout_stripView.setVisibility(View.VISIBLE);
        }else {
            checkout_stripView.setVisibility(View.GONE);
        }
    }

    public static List<OrderDetail> orderItemList(){
        return OrderDetail.findWithQuery(OrderDetail.class, "SELECT * FROM order_detail");
    }

    public static Order getStandingOrder(){
        List<Order> allOrders = Order.findWithQuery(Order.class, "SELECT * FROM order");
        if (allOrders != null && !allOrders.isEmpty()){
            return allOrders.get(0);
        }
        return null;
    }

    public static void saveOrder(List<OrderDetail>orderDetailList){
        Order order = new Order();
        order.setOrderDate(0);
        order.setOrderItem(orderDetailList);
        order.setOrderStatus(0);
//        order.save();
    }

    public static OrderDetail getOrderDetailById(long id){
        return OrderDetail.findById(OrderDetail.class, id);
    }

























































































    //        List<Product> mLIst;
//        mLIst = Product.findWithQuery(Product.class, "SELECT * FROM product");
//        if (mLIst.size() < 1){
//            return mLIst;
//        }else {
//            mLIst = new ArrayList<>();
//            Product product = new Product();
//            product.setProductName("Atlas Strong 20%");
//            product.setProductType("500ml");
//            product.setProductPrice(500);
//            product.save();
//
//            Product product1 = new Product();
//            product1.setProductName("Atlas Strong 20%");
//            product1.setProductType("500ml");
//            product1.setProductPrice(600);
//            product1.save();
//
//            Product product2 = new Product();
//            product2.setProductName("Atlas Strong 20%");
//            product2.setProductType("500ml");
//            product2.setProductPrice(100);
//            product2.save();
//
//            Product product3 = new Product();
//            product3.setProductName("Atlas Strong 20%");
//            product3.setProductType("500ml");
//            product3.setProductPrice(180);
//            product3.save();
//
//            Product product4 = new Product();
//            product4.setProductName("Atlas Strong 20%");
//            product4.setProductType("500ml");
//            product4.setProductPrice(210);
//            product4.save();
//
//            Product product5 = new Product();
//            product5.setProductName("Atlas Strong 20%");
//            product5.setProductType("500ml");
//            product5.setProductPrice(500);
//            product5.save();
//
//            Product product6 = new Product();
//            product6.setProductName("Atlas Strong 20%");
//            product6.setProductType("500ml");
//            product6.setProductPrice(500);
//            product6.save();
//
//            Product product7 = new Product();
//            product7.setProductName("Atlas Strong 20%");
//            product7.setProductType("500ml");
//            product7.setProductPrice(500);
//            product7.save();
//
//            Product product8 = new Product();
//            product8.setProductName("Atlas Strong 20%");
//            product8.setProductType("500ml");
//            product8.setProductPrice(500);
//            product8.save();
//
//            Product product9 = new Product();
//            product9.setProductName("Atlas Strong 20%");
//            product9.setProductType("500ml");
//            product9.setProductPrice(500);
//            product9.save();
//
//            Product product10 = new Product();
//            product10.setProductName("Atlas Strong 20%");
//            product10.setProductType("500ml");
//            product10.setProductPrice(500);
//            product10.save();
//
//            Product productA = new Product();
//            productA.setProductName("Atlas Strong 20%");
//            productA.setProductType("500ml");
//            productA.setProductPrice(500);
//            productA.save();
//
//            Product productB = new Product();
//            productB.setProductName("Atlas Strong 20%");
//            productB.setProductType("500ml");
//            productB.setProductPrice(500);
//            productB.save();
//
//            Product productC = new Product();
//            productC.setProductName("Atlas Strong 20%");
//            productC.setProductType("500ml");
//            productC.setProductPrice(500);
//            productC.save();

//            mLIst.add(product);
//            mLIst.add(product1);
//            mLIst.add(product2);
//            mLIst.add(product3);
//            mLIst.add(product4);
//            mLIst.add(product5);
//            mLIst.add(product6);
//            mLIst.add(product7);
//            mLIst.add(product8);
//            mLIst.add(product9);
//            mLIst.add(product10);
//            mLIst.add(productA);
//            mLIst.add(productB);
//            mLIst.add(productC);
}
